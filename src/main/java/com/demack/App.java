package com.demack;

import static com.demack.jchain.utils.StringHelper.formatHashForLog;

import com.demack.jchain.domain.Block;

/**
 * Hello world!
 */
public class App {
  public static void main(String[] args) {
    Block genesisBlock = new Block(new byte[0], "Genesis Block");
    System.out.println("Hash for block 1 : " + formatHashForLog(genesisBlock.getHash()));

    Block secondBlock = new Block(genesisBlock.getHash(), "Yo im the second block");
    System.out.println("Hash for block 2 : " + formatHashForLog(secondBlock.getHash()));

    Block thirdBlock = new Block(secondBlock.getHash(), "Hey im the third block");
    System.out.println("Hash for block 3 : " + formatHashForLog(thirdBlock.getHash()));
  }
}
