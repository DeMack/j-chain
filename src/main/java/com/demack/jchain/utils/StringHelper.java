package com.demack.jchain.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.StringJoiner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StringHelper {
  private static final Logger LOGGER = LoggerFactory.getLogger(StringHelper.class);

  private final static char[] hexArray = "0123456789ABCDEF".toCharArray();

  public static byte[] calcHash(byte[] bytes, List<String> message) {

    try {
      StringJoiner sj = new StringJoiner("::");
      message.forEach(s -> sj.add(s));

      ByteArrayOutputStream stream = new ByteArrayOutputStream();
      stream.write(bytes);
      stream.write(sj.toString().getBytes(StandardCharsets.UTF_8));
      return calcHash(stream.toByteArray());
    } catch (IOException e) {
      LOGGER.error("Failed to parse message to byte[]", e);
      return new byte[0];
    }
  }

  public static byte[] calcHash(byte[] toHash) {
    try {
      return MessageDigest.getInstance("SHA-256").digest(toHash);
    } catch (NoSuchAlgorithmException e) {
      LOGGER.error("Failed to calculate hash", e);
      return new byte[0];
    }
  }

  public static String formatHashForLog(byte[] hash) {
    // Code copied from https://stackoverflow.com/questions/9655181/how-to-convert-a-byte-array-to-a-hex-string-in-java
    char[] hexChars = new char[hash.length * 2];
    for (int j = 0; j < hash.length; j++) {
      int v = hash[j] & 0xFF;
      hexChars[j * 2] = hexArray[v >>> 4];
      hexChars[j * 2 + 1] = hexArray[v & 0x0F];
    }
    return new String(hexChars);
  }
}
