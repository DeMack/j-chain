package com.demack.jchain.domain;

import static com.demack.jchain.utils.StringHelper.calcHash;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Date;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Block {
  private static final Logger LOGGER = LoggerFactory.getLogger(Block.class);

  private final byte[] hash;
  private final byte[] prevHash;
  private final String data;
  private final long timeStamp;

  private int nonce = 0;

  public Block(byte[] prevHash, String data) {
    this.prevHash = prevHash;
    this.data = data;
    this.timeStamp = new Date().getTime();

    this.hash = calcHash(prevHash, Arrays.asList(data, String.valueOf(timeStamp)));
  }

  public byte[] getHash() {
    return hash;
  }

  public byte[] getPrevHash() {
    return prevHash;
  }

  public String getData() {
    return data;
  }

  public long getTimeStamp() {
    return timeStamp;
  }

  public int getNonce() {
    return nonce;
  }
}
